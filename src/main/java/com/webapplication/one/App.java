package com.webapplication.one;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/hello")
public class App extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public App() {
		super();
	}

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String yourName = req.getParameter("yourName");
		PrintWriter writer = resp.getWriter();
		writer.println("<h1>Hello " + yourName + "</h1>");

		writer.close();
		super.doPost(req, resp);
		super.doPut(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		super.doPost(req, resp);

	}

}
