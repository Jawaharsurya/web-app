package com.webapplication.one;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

public class FilterClass implements Filter {
	private static final Logger log = Logger.getLogger(FilterClass.class);

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

		String testParam = filterConfig.getInitParameter("test-param");

		log.info("Test Param: " + testParam);
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		PrintWriter writer = response.getWriter();
		String yourName = req.getParameter("yourName");
		String ipAddress = request.getRemoteAddr();

		if (StringUtils.isBlank(yourName) || StringUtils.isNumeric(yourName)) {
			log.info("IP " + ipAddress + ", Time " + new Date().toString());
			writer.println("<h1>error</h1>");
		} else {

			log.info("IP " + ipAddress + ", Time " + new Date().toString());
			chain.doFilter(request, response);
			writer.close();

		}
	}

	@Override
	public void destroy() {
	}

}
